var webpack = require('webpack');
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    context: __dirname,
    entry: ['./resources/javascript/main.js'],
    devtool: "inline-sourcemap",
    output: {
        path: `${__dirname}/public/javascript`,
        filename: "main.min.js",
    },
    devServer : {
        inline      : true,
        port        : 3000,
        contentBase : './public'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query:{
                    presets:['es2015', 'stage-0'],
                    plugins:['transform-runtime']//ie generators
                }
            }
        ]
    },
    plugins:[
        new LiveReloadPlugin({})
    ],
};
