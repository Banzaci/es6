import { createServer } from 'http';

createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hey');
}).listen(3000, '127.0.0.1');
