//ex1:
const rebuild = ({ name:username, img='default.jpg' }) => Object.assign({}, { username, img });

//ex2:
const welcomeMessage = ({ username }) => `Hi ${username} and welcome to the djungle!`;

//ex3.
const removeDuplicatedArray = (arr) => {
    return arr.reduce((acc, current) => {
        if(acc.indexOf(current) === -1) {
            acc.push(current)
        };
        return acc;
    }, []);
}

//ex4.
const insertArrayInArray = (arr) => [].concat([0, ...arr, 100]);

//ex5.
const concatinateManyArrays = (...arrs)=> {
  return [...arrs];
}
concatinateManyArrays(...arr1, ...arr2, ...arr3);
