function flatten( objToFlatten, flattened={}, namespaceKey='' ){
    const keys = Reflect.ownKeys(objToFlatten);
    return keys.reduce((acc, item) => {
        if(objToFlatten[item] instanceof Object){
            return flatten(objToFlatten[item], flattened, item);
        } else {
            let namespace = ( namespaceKey === '' ) ? item : namespaceKey + '.' + item;
            acc[namespace] = objToFlatten[item];
            return acc;
        }
    }, flattened)
    // keys.forEach((item) => {
    //     if(objToFlatten[item] instanceof Object){
    //         flatten(objToFlatten[item], flattened, item);
    //     } else {
    //         let namespace = ( namespaceKey === '' ) ? item : namespaceKey + '.' + item;
    //         flattened[namespace] = objToFlatten[item];
    //     }
    // });
    //return flattened;
}

export const flatObject = ( objToFlatten ) => Object.assign({}, flatten(objToFlatten));
