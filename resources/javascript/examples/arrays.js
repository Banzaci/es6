function flatten( arr ){
    function* flattenArray() {
        for( let element of arr ) {
            if ( Array.isArray( element )) {
                yield* flatten( element );
            } else {
                yield element;
            }
        }
    }
    return [...flattenArray()];
}

function queArray( noOfItems = 10 ){
    let container = [];
    return (message) => {
        if(container.length >= noOfItems) {
            container.shift();
        }
        container.push(message);
        return [].concat(container);
    }
}

const sumReduce = (arr) => arr.reduce((acc, current) => acc + current);
const sortReduce = (arr) => [].concat(arr).sort((acc, current) => acc - current);//concat needed, otherwise change origional!

export const sort       = ( arr ) => sortReduce(arr);
export const sum        = ( arr ) => sumReduce(arr);
export const flatMap    = ( arr ) => flatten(arr);
export const que        = ( arr ) => queArray(arr);
