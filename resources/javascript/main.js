import { flatMap, sort, que } from './examples/arrays';
import { flatObject } from './examples/objects';

import rebuild from './ex1';
import welcomeMessage from './ex2';
import removeDuplicatesArray from './ex3';
import insertArrayInArray from './ex4';
import concatinateManyArrays from './ex5';


const arr1 = [1,2,3];
const arr2 = ['4','5','6'];
const arr3 = [7,8,9];

const arr = [5, [6,3, 25, [4,1]], 2, 6, 1];
const obj = {
    name : 'Michael Larsson',
    address : {
        street : 'Dalagatan 44',
        zip : '11324',
        contact : {
            mobile  : 123456,
            fax     : 123456,
            email   : 'sv@miche.se'
        }
    }
};

// let tempArr = [];
// for(let i = 0; i <10000; i++){
//     tempArr.push(Math.round(Math.random(i)*1000)+i);
// }

const flattenedArray        = flatMap(arr);
const queArray              = que(3);
const flattenedObject       = flatObject(obj);
const userObject            = rebuild(obj);
const userWelcomeMessage    = welcomeMessage(userObject);
const sortedArray           = sort(flattenedArray);

//console.time('timeTestSortingArray');
const timeTestSortingArray  = sort(sortedArray);
//console.timeEnd('timeTestSortingArray');
const concatinateArrays     = concatinateManyArrays(arr1, arr2, arr3);
const removeDuplicates      = removeDuplicatesArray(sortedArray);
const insertIntoArray       = insertArrayInArray(removeDuplicates);

// for(let i = 0; i < 10; i++) {
//     console.log(queArray(`hello ${i+1}`));
// }
// console.log(queArray('hello 11'));
// console.log(queArray('hello 12'));
// console.log(flattenedArray);
console.log(flattenedObject);
// console.log(sortedArray);
// console.log(userObject);
// console.log(userWelcomeMessage);
// console.log(removeDuplicates);
// console.log(insertIntoArray);
// console.log(concatinateArrays);
