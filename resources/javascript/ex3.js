//ex3 use reduce.
const removeDuplicatedArray = (arr) => {
    var newArr = [];
    arr.forEach(function(item){
        if(newArr.indexOf(item) === -1){
            newArr.push(item);
        }
    });
    return newArr;
}

export default removeDuplicatedArray;
