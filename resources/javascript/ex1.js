//Rebuild function to es2015 style!!! Deconstructing

function rebuild( data ){

    var username = data.name;
    var img = data.img ? data.img : 'default.jpg';

    return {
        username : username,
        img : img
    }
}


export default rebuild;
